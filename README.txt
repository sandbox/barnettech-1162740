README.txt
____________________
Submitted by Barnettech www.barnettech.com  (info@barnettech.com or james_barnett@barnettech.com) 5/19/2011
Module was originally created for Babson College

DESCRIPTION
____________________
Facebook Wall Functionality. This module uses the user relationship module (a dependency), and any friends that you have on the site who post with a content type of "wall" will be able to post and comment on your wall.

INSTALLATION
____________________

To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the the_wall directory and all its contents to your modules directory.

3. Visit admin/modules and enable the "The Wall" module and the user relationship module which is a dependency (you will need to download this from Drupal.org as well and put it in your modules directory as well).

3.5.  Setup a content type and call it "Wall" with a machine name of "wall".

4. Configure the user relationship module so that there is a reciprocal friend (friends is the plural in the setup) relationship setup.

5. When you post on your wall, or post a piece of content of content type wall, it will show up on your wall and on the wall(s) of your friend(s). Once an item shows up on your wall you can comment on the item.

6. The Wall makes use of nodes, and node comments, and all features available within this well established node framework are available to you.  You can post to your wall with the quick ajax textarea box available or click on the link above it for the full submittal form to submit a node of type "wall".  If you have pictures enabled for the content type, your users will be able to submit pictures.  It is a content type like any other so views can be leveraged which some may find valuable.

7. The picture size for nodes (actual posts on the wall), and the picture size for those who leave comments is now configurable under:  config/media/the_wall. The image styles that show up in the radio list can be configured here:  config/media/image-styles.  The default is it uses the thumbnail size for both comments and posts.                                                                                                                                                                                                               ~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
~                                                                                                                                                                                                               
Type  :quit<Enter>  to exit Vim
